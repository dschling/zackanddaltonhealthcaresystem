﻿namespace HealthcareAssignment.Model
{
    internal class RoutineCheck
    {
        #region Properties

        public int AppointmentID { get; set; }

        public int NurseId { get; set; }

        public decimal BodyTemperature { get; set; }

        public int Systolic { get; set; }

        public int Diastolic { get; set; }

        public int Pulse { get; set; }

        #endregion
    }
}