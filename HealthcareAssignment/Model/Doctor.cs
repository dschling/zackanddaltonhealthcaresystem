﻿namespace HealthcareAssignment.Model
{
    public class Doctor : Person
    {
        #region Properties

        public int DoctorId { get; set; }

        #endregion

        #region Constructors

        public Doctor(int id, string fname, string lname) : base(fname, lname)
        {
            this.DoctorId = id;
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "Dr. " + FirstName + " " + LastName;
        }

        #endregion
    }
}