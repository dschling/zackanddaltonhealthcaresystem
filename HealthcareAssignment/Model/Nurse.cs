﻿namespace HealthcareAssignment.Model
{
    internal class Nurse : Person
    {
        #region Properties

        public int NurseId { get; set; }

        #endregion

        #region Constructors

        public Nurse(string fname, string lname, int id) : base(fname, lname)
        {
            this.NurseId = id;
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }

        #endregion
    }
}