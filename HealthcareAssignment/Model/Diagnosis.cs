﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthcareAssignment.Model
{
    class Diagnosis
    {

        public int AppointmentId { get; set; }

        public string InitialDiagnosis { get; set; }

        public string FinalDiagnosis { get; set; }


    }
}
