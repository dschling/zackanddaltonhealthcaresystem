﻿using System;

namespace HealthcareAssignment.Model
{
    internal class Appointment
    {
        #region Properties

        public int AppointmentID { get; set; }

        public int DoctorID { get; set; }

        public string DoctorName { get; set; }

        public int PatientID { get; set; }

        public DateTime AppointmentDate { get; set; }

        public string Reason { get; set; }

        #endregion
    }
}