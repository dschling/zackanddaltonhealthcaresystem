﻿namespace HealthcareAssignment.Model
{
    public class Person
    {
        #region Properties

        public string FirstName { get; set; }

        public string LastName { get; set; }

        #endregion

        #region Constructors

        protected Person(string fname, string lname)
        {
            this.FirstName = fname;
            this.LastName = lname;
        }

        protected Person()
        {
        }

        #endregion
    }
}