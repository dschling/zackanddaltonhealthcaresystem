﻿using System;

namespace HealthcareAssignment.Model
{
    public class Patient : Person
    {
        #region Properties

        public int PatientId { get; set; }

        public DateTime Birthdate { get; set; }

        public char Gender { get; set; }

        public string Street { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public int Zip { get; set; }

        public long PhoneNumber { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Patient" /> class.
        /// </summary>
        public Patient(string fname, string lname) : base(fname, lname)
        {
            this.Birthdate = DateTime.MinValue;
        }

        public Patient()
        {
        }

        #endregion
    }
}