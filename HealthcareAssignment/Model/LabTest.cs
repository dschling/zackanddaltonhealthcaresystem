﻿using System;

namespace HealthcareAssignment.Model
{
    public class LabTest
    {
        #region Properties

        public int AppointmentId { get; set; }

        public string TestCode { get; set; }

        public string Result { get; set; }

        public DateTime TestDate { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="LabTest" /> class.
        /// </summary>
        /// <param name="appointment">The appointment.</param>
        /// <param name="testCode">The test code.</param>
        public LabTest(int appointment, string testCode)
        {
            this.AppointmentId = appointment;
            this.TestCode = testCode;
        }

        #endregion
    }
}