﻿using System.Collections.Generic;

namespace HealthcareAssignment.DAL.Interfaces
{
    internal interface IRepository<T>
    {
        #region Methods

        /// <summary>
        ///     Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Add(T entity);

        /// <summary>
        ///     Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        T GetById(int id);

        /// <summary>
        ///     Gets all items in the list.
        /// </summary>
        /// <returns>All list items</returns>
        IList<T> GetAll();

        /// <summary>
        ///     Searches the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The entity.</returns>
        IList<T> Search(T entity);

        #endregion
    }
}