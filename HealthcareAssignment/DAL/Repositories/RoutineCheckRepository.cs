﻿using System;
using System.Collections.Generic;
using System.Configuration;
using HealthcareAssignment.DAL.Interfaces;
using HealthcareAssignment.Model;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.DAL.Repositories
{
    internal class RoutineCheckRepository : IRepository<RoutineCheck>
    {
        #region Data members

        private readonly string connectionLabel;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="RoutineCheckRepository" /> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public RoutineCheckRepository(string connection = "MySqlDbConnection")
        {
            var connStr = ConfigurationManager.ConnectionStrings[connection].ConnectionString;
            this.connectionLabel = connStr;
        }

        #endregion

        #region Methods

        public void Add(RoutineCheck entity)
        {
            try
            {
                using (var conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd =
                            new MySqlCommand(
                                "INSERT into ROUTINE_CHECK (appointmentID,nurseID,bodyTemp,systolic,diastolic,pulse) VALUES (@appointmentID, @nurseID, @bodyTemp, @systolic, @diastolic, @pulse)",
                                conn))
                    {
                        cmd.Parameters.Add("@appointmentID", MySqlDbType.Int32, 11);
                        cmd.Parameters.Add("@nurseID", MySqlDbType.Int32, 11);
                        cmd.Parameters.Add("@bodyTemp", MySqlDbType.Decimal);
                        cmd.Parameters.Add("@systolic", MySqlDbType.Int32, 11);
                        cmd.Parameters.Add("@diastolic", MySqlDbType.Int32, 11);
                        cmd.Parameters.Add("@pulse", MySqlDbType.Int32, 11);
                        cmd.Parameters["@appointmentID"].Value = entity.AppointmentID;
                        cmd.Parameters["@nurseID"].Value = entity.NurseId;
                        cmd.Parameters["@bodyTemp"].Value = entity.BodyTemperature;
                        cmd.Parameters["@systolic"].Value = entity.Systolic;
                        cmd.Parameters["@diastolic"].Value = entity.Diastolic;
                        cmd.Parameters["@pulse"].Value = entity.Pulse;

                        cmd.ExecuteNonQuery();
                    }
                }
            }

            catch (MySqlException mex)
            {
                Console.WriteLine(mex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public RoutineCheck GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IList<RoutineCheck> GetAll()
        {
            throw new NotImplementedException();
        }

        public IList<RoutineCheck> Search(RoutineCheck entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Gets the name of the i ds by column.
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
        public Dictionary<DateTime, int> GetAppointmentDates(int patientID)
        {
            var dates = new Dictionary<DateTime, int>();
            var query = "SELECT * FROM APPOINTMENT WHERE patientID = @patientID";

            try
            {
                using (var conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd = new MySqlCommand(query, conn))
                    {
                        cmd.Parameters.Add("@patientID", MySqlDbType.Int32, 11);
                        cmd.Parameters["@patientID"].Value = patientID;
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                dates.Add(reader.GetDateTime("appointmentDate"), reader.GetInt32("appointmentID"));
                            }
                        }
                    }
                }
            }

            catch (MySqlException mex)
            {
                Console.WriteLine(mex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return dates;
        }

        public List<Nurse> GetNurses()
        {
            var nurses = new List<Nurse>();
            var query = "SELECT * FROM EMPLOYEE as e, NURSE as n WHERE e.employeeID = nurseID";

            try
            {
                using (var conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd = new MySqlCommand(query, conn))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var nurse = new Nurse(reader.GetString("firstName"), reader.GetString("lastName"),
                                    reader.GetInt32("nurseID"));
                                nurses.Add(nurse);
                            }
                        }
                    }
                }
            }

            catch (MySqlException mex)
            {
                Console.WriteLine(mex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return nurses;
        }

        #endregion
    }
}