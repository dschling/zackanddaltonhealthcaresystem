﻿using System;
using System.Collections.Generic;
using System.Configuration;
using HealthcareAssignment.DAL.Interfaces;
using HealthcareAssignment.Model;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.DAL.Repositories
{
    internal class AppointmentRepository : IRepository<Appointment>

    {
        #region Data members

        private readonly string connectionLabel;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="PatientRepository" /> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public AppointmentRepository(string connection = "MySqlDbConnection")
        {
            var connStr = ConfigurationManager.ConnectionStrings[connection].ConnectionString;
            this.connectionLabel = connStr;
        }

        #endregion

        #region Methods

        public void Add(Appointment entity)
        {
            try
            {
                using (var conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd =
                            new MySqlCommand(
                                "INSERT into APPOINTMENT (patientID,doctorID,appointmentDate,reason) VALUES (@patientID, @doctorID, @appointmentDate, @reason)",
                                conn))
                    {
                        cmd.Parameters.Add("@patientID", MySqlDbType.Int32, 11);
                        cmd.Parameters.Add("@doctorID", MySqlDbType.Int32, 11);
                        cmd.Parameters.Add("@appointmentDate", MySqlDbType.DateTime);
                        cmd.Parameters.Add("@reason", MySqlDbType.VarString, 100);
                        cmd.Parameters["@patientID"].Value = entity.PatientID;
                        cmd.Parameters["@doctorID"].Value = entity.DoctorID;
                        cmd.Parameters["@appointmentDate"].Value = entity.AppointmentDate;
                        cmd.Parameters["@reason"].Value = entity.Reason;

                        cmd.ExecuteNonQuery();
                    }
                }
            }

            catch (MySqlException mex)
            {
                Console.WriteLine(mex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Appointment GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IList<Appointment> GetAll()
        {
            throw new NotImplementedException();
        }

        public IList<Appointment> Search(Appointment entity)
        {
            throw new NotImplementedException();
        }

        public List<Doctor> GetDoctorIDs()
        {
            var doctors = new List<Doctor>();

            try
            {
                using (var conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd = new MySqlCommand("SELECT * FROM EMPLOYEE, DOCTOR WHERE employeeID = doctorID", conn))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                doctors.Add(new Doctor(reader.GetInt32("doctorID"), reader.GetString("firstName"),
                                    reader.GetString("lastName")));
                            }
                        }
                    }
                }
            }

            catch (MySqlException mex)
            {
                throw mex;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return doctors;
        }

        #endregion
    }
}