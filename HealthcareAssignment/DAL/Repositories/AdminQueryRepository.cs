﻿using System;
using System.Configuration;
using System.Data;
using HealthcareAssignment.Properties;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.DAL.Repositories
{
    internal class AdminQueryRepository
    {
        #region Data members

        private readonly string connectionLabel;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AdminQueryRepository" /> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public AdminQueryRepository(string connection = "MySqlDbConnection")
        {
            var connStr = ConfigurationManager.ConnectionStrings[connection].ConnectionString;
            this.connectionLabel = connStr;
        }

        #endregion

        /// <summary>
        ///     Gets the data from query.
        /// </summary>
        /// <param name="sqlCommand">The SQL command.</param>
        /// <returns>DataTable of query result</returns>
        /// <exception cref="MySqlException">Throws MySqlException</exception>
        public DataTable GetDataFromQuery(string sqlCommand)
        {
            var table = new DataTable();
            var connection = new MySqlConnection(this.connectionLabel);

            connection.Open();
            var command = new MySqlCommand(sqlCommand, connection);
            var adapter = new MySqlDataAdapter();
            adapter.SelectCommand = command;

            adapter.Fill(table);

            return table;
        }

        /// <summary>
        /// Gets the data in time period.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <returns>DataTable</returns>
        /// <exception cref="MySqlException">Throws MySqlException</exception>
        public DataTable GetDataInTimePeriod(DateTime start, DateTime end)
        {
            var table = new DataTable();
            table.Columns.Add("Appointment Date");
            table.Columns.Add("Patient ID");
            table.Columns.Add("Patient Name");
            table.Columns.Add("Doctor Name");
            table.Columns.Add("Nurse Name");
            table.Columns.Add("Lab Tests");
            table.Columns.Add("Diagnoses");

            using (var conn = new MySqlConnection(this.connectionLabel))
            {
                conn.Open();

                using (var cmd = new MySqlCommand(Resources.datesQuery, conn))
                {
                    cmd.Parameters.Add("@start", MySqlDbType.DateTime);
                    cmd.Parameters.Add("@end", MySqlDbType.DateTime);
                    cmd.Parameters["@start"].Value = start;
                    cmd.Parameters["@end"].Value = end;
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var date = reader.GetDateTime("appointmentDate");
                            var patientId = reader.GetInt32("patientID");
                            var tests = this.getLabTestsForAppointment(date, patientId);
                            var patient = reader.GetString("Patient");
                            var doctor = reader.GetString("Doctor");
                            var nurse = reader.GetString("Nurse");
                            var diagnoses = reader.GetString("finalDiagnosis");

                            object[] row = {
                                date.ToShortDateString(),
                                patientId,
                                patient,
                                doctor,
                                nurse,
                                tests,
                                diagnoses
                            };
                            table.Rows.Add(row);
                        }
                    }
                }
            }
            return table;
        }

        private string getLabTestsForAppointment(DateTime date, int patientId)
        {
            var tests = "";
            using (var conn = new MySqlConnection(this.connectionLabel))
            {
                conn.Open();
                
                using (var cmd = new MySqlCommand("sp_GetEachLabTestForEachAppointment", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("aDate", date);
                    cmd.Parameters.AddWithValue("pID", patientId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tests += reader.GetString("testCode") + ": ";
                            if (reader.IsDBNull(reader.GetOrdinal("result")))
                            {
                                tests += "No result, ";
                            }
                            else
                            {
                                tests += reader.GetString("result") + ", ";
                            }
                        }
                    }
                }
            }
            return tests;
        }
    }
}