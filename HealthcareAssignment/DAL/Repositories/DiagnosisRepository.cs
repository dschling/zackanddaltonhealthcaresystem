﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareAssignment.DAL.Interfaces;
using HealthcareAssignment.Model;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.DAL.Repositories
{
    class DiagnosisRepository : IRepository<Diagnosis>
    {

        private readonly string connectionLabel;

        public DiagnosisRepository(string connection = "MySqlDbConnection")
        {
            var connStr = ConfigurationManager.ConnectionStrings[connection].ConnectionString;
            this.connectionLabel = connStr;
        }

        public void Add(Diagnosis entity)
        {
            using (var conn = new MySqlConnection(this.connectionLabel))
            {
                conn.Open();

                using (
                    var cmd =
                        new MySqlCommand(
                            "INSERT into DIAGNOSES (appointmentID,initialDiagnosis,finalDiagnosis) VALUES (@appointmentID, @initialDiagnosis, @finalDiagnosis)",
                            conn))
                {
                    cmd.Parameters.Add("@appointmentID", MySqlDbType.Int32, 11);
                    cmd.Parameters.Add("@initialDiagnosis", MySqlDbType.String);
                    cmd.Parameters.Add("@finalDiagnosis", MySqlDbType.String);
                    cmd.Parameters["@appointmentID"].Value = entity.AppointmentId;
                    cmd.Parameters["@initialDiagnosis"].Value = entity.InitialDiagnosis;
                    cmd.Parameters["@finalDiagnosis"].Value = entity.FinalDiagnosis;

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public Diagnosis GetById(int appointmentID)
        {
            var diagnosis = new Diagnosis();
            using (var conn = new MySqlConnection(this.connectionLabel))
            {
                conn.Open();

                using (var cmd = new MySqlCommand("sp_GetDiagnosisByAppointment", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("aID", appointmentID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            diagnosis.AppointmentId = appointmentID;
                            diagnosis.InitialDiagnosis = reader.GetString("initialDiagnosis");
                            if (reader.IsDBNull(2))
                            {
                                diagnosis.FinalDiagnosis = string.Empty;
                            }
                            else
                            {
                                diagnosis.FinalDiagnosis = reader.GetString("finalDiagnosis"); 
                            }
                            
                        }
                    }
                }
            }
            return diagnosis;
        }

        public void AddFinalDiagnosis(int appointmentID, string finalDiagnosis)
        {
            using (var conn = new MySqlConnection(this.connectionLabel))
            {
                conn.Open();

                using (
                    var cmd =
                        new MySqlCommand(
                            "UPDATE DIAGNOSES SET finalDiagnosis=@finalDiagnosis WHERE appointmentID=@appointmentID",
                            conn))
                {
                    cmd.Parameters.Add("@appointmentID", MySqlDbType.Int32, 11);
                    cmd.Parameters.Add("@finalDiagnosis", MySqlDbType.String);
                    cmd.Parameters["@appointmentID"].Value = appointmentID;
                    cmd.Parameters["@finalDiagnosis"].Value = finalDiagnosis;

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public IList<Diagnosis> GetAll()
        {
            throw new NotImplementedException();
        }

        public IList<Diagnosis> Search(Diagnosis entity)
        {
            throw new NotImplementedException();
        }
    }
}
