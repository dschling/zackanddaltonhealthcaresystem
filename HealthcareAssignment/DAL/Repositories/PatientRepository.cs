﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using HealthcareAssignment.DAL.Interfaces;
using HealthcareAssignment.Model;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.DAL.Repositories
{
    internal class PatientRepository : IRepository<Patient>
    {
        #region Data members

        private readonly string connectionLabel;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="PatientRepository" /> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public PatientRepository(string connection = "MySqlDbConnection")
        {
            var connStr = ConfigurationManager.ConnectionStrings[connection].ConnectionString;
            this.connectionLabel = connStr;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds the specified patient to the db.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="MySqlException">Throws MySqlException</exception>
        public void Add(Patient entity)
        {
            using (var conn = new MySqlConnection(this.connectionLabel))
            {
                conn.Open();

                using (
                    var cmd =
                        new MySqlCommand(
                            "INSERT into PATIENT (firstName,lastName,dob,gender,street,city,state,zip,phoneNumber) VALUES (@firstName,@lastName,@dob,@gender,@street,@city,@state,@zip,@phoneNumber)",
                            conn))
                {
                    cmd.Parameters.Add("@firstName", MySqlDbType.String, 45);
                    cmd.Parameters.Add("@lastName", MySqlDbType.String, 45);
                    cmd.Parameters.Add("@dob", MySqlDbType.DateTime);
                    cmd.Parameters.Add("@gender", MySqlDbType.VarChar, 1);
                    cmd.Parameters.Add("@street", MySqlDbType.String, 45);
                    cmd.Parameters.Add("@city", MySqlDbType.String, 45);
                    cmd.Parameters.Add("@state", MySqlDbType.String, 45);
                    cmd.Parameters.Add("@zip", MySqlDbType.Int32, 11);
                    cmd.Parameters.Add("@phoneNumber", MySqlDbType.Int64);
                    cmd.Parameters["@firstName"].Value = entity.FirstName;
                    cmd.Parameters["@lastName"].Value = entity.LastName;
                    cmd.Parameters["@dob"].Value = entity.Birthdate;
                    cmd.Parameters["@gender"].Value = entity.Gender;
                    cmd.Parameters["@street"].Value = entity.Street;
                    cmd.Parameters["@city"].Value = entity.City;
                    cmd.Parameters["@state"].Value = entity.State;
                    cmd.Parameters["@zip"].Value = entity.Zip;
                    cmd.Parameters["@phoneNumber"].Value = entity.PhoneNumber;

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Searches the specified patient.
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <returns>A list of patients</returns>
        /// <exception cref="MySqlException">Throws MySqlException</exception>
        public IList<Patient> Search(Patient patient)
        {
            IList<Patient> patients = new List<Patient>();
            var query = this.determineSelectQuery(patient);

                using (var conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (var cmd = new MySqlCommand(query, conn))
                    {
                        cmd.Parameters.Add("@firstName", MySqlDbType.String, 45);
                        cmd.Parameters.Add("@lastName", MySqlDbType.String, 45);
                        cmd.Parameters.Add("@dob", MySqlDbType.DateTime);

                        cmd.Parameters["@firstName"].Value = patient.FirstName;
                        cmd.Parameters["@lastName"].Value = patient.LastName;
                        cmd.Parameters["@dob"].Value = patient.Birthdate;

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var newPatient = new Patient(reader.GetString("firstName"), reader.GetString("lastName")) {
                                    PatientId = reader.GetInt32("patientID"),
                                    Birthdate = reader.GetDateTime("dob"),
                                    Street = reader.GetString("street"),
                                    City = reader.GetString("city"),
                                    State = reader.GetString("state"),
                                    Zip = reader.GetInt32("zip"),
                                    Gender = reader.GetChar("gender"),
                                    PhoneNumber = reader.GetInt64("phoneNumber")
                                };
                                patients.Add(newPatient);
                            }
                        }
                    }
                }
            

            return patients;
        }

        public IList<Patient> GetAll()
        {
            throw new NotImplementedException();
        }

        public Patient GetById(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        public IList<LabTest> GetTests(Patient patient, int appointmentID)
        {
            var tests = new List<LabTest>();

            using (var conn = new MySqlConnection(this.connectionLabel))
            {
                conn.Open();

                using (var cmd = new MySqlCommand("sp_GetLabTestsByAppointment", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("pID", patient.PatientId);
                    cmd.Parameters.AddWithValue("aID", appointmentID);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var newTest = new LabTest(reader.GetInt32("appointmentID"), reader.GetString("testCode"));
                            if (reader.IsDBNull(2))
                            {
                                newTest.Result = string.Empty;
                            }
                            else
                            {
                                newTest.Result = reader.GetString("result");
                            }
                            newTest.TestDate = reader.GetDateTime("datePerformed");

                            tests.Add(newTest);
                        }
                    }
                }
            }
            return tests;
        }

        /// <summary>
        ///     Adds the lab test to the db.
        /// </summary>
        /// <param name="test">The test.</param>
        /// <exception cref="MySqlException"> Throws MySqlException </exception>
        public void AddLabTest(LabTest test)
        {
            try
            {
                using (var conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd =
                            new MySqlCommand(
                                "INSERT into LABTEST (appointmentID,testCode,result,datePerformed) VALUES (@appointmentID, @testCode, @result, @datePerformed)",
                                conn))
                    {
                        cmd.Parameters.Add("@appointmentID", MySqlDbType.Int32, 11);
                        cmd.Parameters.Add("@testCode", MySqlDbType.String);
                        cmd.Parameters.Add("@result", MySqlDbType.String);
                        cmd.Parameters.Add("@datePerformed", MySqlDbType.DateTime);
                        cmd.Parameters["@appointmentID"].Value = test.AppointmentId;
                        cmd.Parameters["@testCode"].Value = test.TestCode;
                        cmd.Parameters["@result"].Value = test.Result;
                        cmd.Parameters["@datePerformed"].Value = test.TestDate;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException e)
            {
                throw e;
            }
        }

        public void UpdateTestResult(LabTest test)
        {
            try
            {
                using (var conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd =
                            new MySqlCommand(
                                "UPDATE LABTEST SET result=@result, datePerformed=@datePerformed WHERE appointmentID=@appointmentID AND testCode=@testCode",
                                conn))
                    {
                        cmd.Parameters.Add("@appointmentID", MySqlDbType.Int32, 11);
                        cmd.Parameters.Add("@result", MySqlDbType.String);
                        cmd.Parameters.Add("@testCode", MySqlDbType.String);
                        cmd.Parameters.Add("@datePerformed", MySqlDbType.DateTime);
                        cmd.Parameters["@appointmentID"].Value = test.AppointmentId;
                        cmd.Parameters["@result"].Value = test.Result;
                        cmd.Parameters["@testCode"].Value = test.TestCode;
                        cmd.Parameters["@datePerformed"].Value = test.TestDate;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        private string determineSelectQuery(Patient patient)
        {
            var query = "";

            if (patient.FirstName != null && patient.LastName != null && patient.Birthdate == DateTime.MinValue)
            {
                query = "SELECT * FROM PATIENT WHERE firstName=@firstName AND lastName=@lastName";
            }
            else if (patient.FirstName == null && patient.LastName == null && patient.Birthdate != DateTime.MinValue)
            {
                query = "SELECT * FROM PATIENT WHERE dob=@dob";
            }
            else if (patient.FirstName != null && patient.LastName == null && patient.Birthdate == DateTime.MinValue)
            {
                query = "SELECT * FROM PATIENT WHERE firstName=@firstName";
            }
            else if (patient.FirstName == null && patient.LastName != null && patient.Birthdate == DateTime.MinValue)
            {
                query = "SELECT * FROM PATIENT WHERE lastName=@lastName";
            }
            else if (patient.FirstName != null && patient.LastName == null && patient.Birthdate != DateTime.MinValue)
            {
                query = "SELECT * FROM PATIENT WHERE firstName=@firstName AND dob=@dob";
            }
            else if (patient.FirstName == null && patient.LastName != null && patient.Birthdate != DateTime.MinValue)
            {
                query = "SELECT * FROM PATIENT WHERE lastName=@lastName AND dob=@dob";
            }
            else if (patient.FirstName != null && patient.LastName != null && patient.Birthdate != DateTime.MinValue)
            {
                query = "SELECT * FROM PATIENT WHERE firstName=@firstName AND lastName=@lastName and dob=@dob";
            }
            return query;
        }
    }
}