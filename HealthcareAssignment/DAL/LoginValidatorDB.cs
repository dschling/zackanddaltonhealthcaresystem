﻿using System;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.DAL
{
    internal class LoginValidatorDB
    {
        #region Data members

        private readonly string connectionLabel;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="LoginValidatorDB" /> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public LoginValidatorDB(string connection = "MySqlDbConnection")
        {
            var connStr = ConfigurationManager.ConnectionStrings[connection].ConnectionString;
            this.connectionLabel = connStr;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Authenticates the user with the DB.
        /// </summary>
        /// <param name="userID">The user identifier.</param>
        /// <param name="password">The password.</param>
        /// <returns>
        ///     <c>true</c> if [is valid user]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsValidUser(int userID, string password)
        {
            var nurseQuery = "select * from NURSE as n where n.nurseID = @userID AND n.nursePassword = @password";
            var adminQuery = "select * from ADMINISTRATOR where adminID = @userID AND adminPassword = @password";

            return this.hasFoundUser(nurseQuery, userID, password) || this.hasFoundUser(adminQuery, userID, password);
        }

        public bool IsAdmin(int userId, string password)
        {
            var query = "select * from ADMINISTRATOR where adminID = @userID AND adminPassword = @password";
            return this.hasFoundUser(query, userId, password);
        }

        private bool hasFoundUser(string command, int userID, string password)
        {
            var isValid = true;
            try
            {
                using (var conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd = new MySqlCommand(command, conn))
                    {
                        cmd.Parameters.Add("@userID", MySqlDbType.Int32, 11);
                        cmd.Parameters.Add("@password", MySqlDbType.String, 45);
                        cmd.Parameters["@userID"].Value = userID;
                        cmd.Parameters["@password"].Value = password;

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (!reader.HasRows)
                            {
                                isValid = false;
                            }
                        }
                    }
                }
            }

            catch (MySqlException mex)
            {
                Console.WriteLine(mex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return isValid;
        }

        /// <summary>
        ///     Gets the name of the current user.
        /// </summary>
        /// <param name="userID">The user identifier.</param>
        /// <returns></returns>
        public string GetCurrentUserName(int userID)
        {
            var name = "";
            try
            {
                using (var conn = new MySqlConnection(this.connectionLabel))
                {
                    conn.Open();

                    using (
                        var cmd =
                            new MySqlCommand(
                                "select firstName, lastName from EMPLOYEE where employeeID = @userID",
                                conn))
                    {
                        cmd.Parameters.Add("@userID", MySqlDbType.Int32, 11);
                        cmd.Parameters["@userID"].Value = userID;

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                name = reader.GetString("firstName") + " " + reader.GetString("lastName");
                            }
                        }
                    }
                }
            }

            catch (MySqlException mex)
            {
                Console.WriteLine(mex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return name;
        }

        #endregion
    }
}