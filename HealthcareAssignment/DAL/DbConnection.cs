﻿using System.Configuration;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.DAL
{
    internal class DbConnection
    {
        #region Methods

        /// <summary>
        ///     Gets the connection.
        /// </summary>
        /// <param name="connlabel">The connlabel.</param>
        /// <returns></returns>
        public static MySqlConnection GetConnection(string connlabel)
        {
            var connection = ConfigurationManager.ConnectionStrings[connlabel].ConnectionString;

            return new MySqlConnection(connection);
        }

        #endregion
    }
}