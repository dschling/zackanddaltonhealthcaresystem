﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareAssignment.DAL.Repositories;

namespace HealthcareAssignment.Controller
{
    class AdminQueryController
    {

        private AdminQueryRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminQueryController"/> class.
        /// </summary>
        public AdminQueryController()
        {
            this.repo = new AdminQueryRepository();
        }

        /// <summary>
        /// Gets the data from query.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public DataTable GetDataFromQuery(string command)
        {
            return this.repo.GetDataFromQuery(command);
        }

        public DataTable GetDataInTimePeriod(DateTime start, DateTime end)
        {
            return this.repo.GetDataInTimePeriod(start, end);
        }
    }
}
