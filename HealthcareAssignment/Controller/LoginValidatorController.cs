﻿using HealthcareAssignment.DAL;

namespace HealthcareAssignment.Controller
{
    internal class LoginValidatorController
    {
        #region Data members

        private readonly LoginValidatorDB validator;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="LoginValidatorController" /> class.
        /// </summary>
        public LoginValidatorController()
        {
            this.validator = new LoginValidatorDB();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Determines whether [is valid user] [the specified user identifier].
        /// </summary>
        /// <param name="userID">The user identifier.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public bool IsValidUser(int userID, string password)
        {
            return this.validator.IsValidUser(userID, password);
        }

        /// <summary>
        ///     Gets the name of the current user.
        /// </summary>
        /// <param name="userID">The user identifier.</param>
        /// <returns></returns>
        public string GetCurrentUserName(int userID)
        {
            return this.validator.GetCurrentUserName(userID);
        }

        public bool IsAdmin(int userId, string password)
        {
            return this.validator.IsAdmin(userId, password);
        }

        #endregion
    }
}