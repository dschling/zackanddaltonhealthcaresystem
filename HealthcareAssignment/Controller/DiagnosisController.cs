﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthcareAssignment.DAL.Repositories;
using HealthcareAssignment.Model;

namespace HealthcareAssignment.Controller
{
    class DiagnosisController
    {

        private readonly DiagnosisRepository repo;

        public DiagnosisController()
        {
            this.repo = new DiagnosisRepository();
        }

        public Diagnosis GetDiagnosis(int appointmentID)
        {
            return this.repo.GetById(appointmentID);
        }

        public void AddDiagnosis(Diagnosis diagnosis)
        {
            this.repo.Add(diagnosis);
        }

        public void AddFinalDiagnosis(int appointmentID, string finalDiagnosis)
        {
            this.repo.AddFinalDiagnosis(appointmentID, finalDiagnosis);
        }
    }
}
