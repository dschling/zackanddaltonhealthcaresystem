﻿using System.Collections.Generic;
using HealthcareAssignment.DAL.Repositories;
using HealthcareAssignment.Model;

namespace HealthcareAssignment.Controller
{
    public class PatientRepositoryController
    {
        #region Data members

        private readonly PatientRepository repo;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="PatientRepositoryController" /> class.
        /// </summary>
        public PatientRepositoryController()
        {
            this.repo = new PatientRepository();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Adds the patient.
        /// </summary>
        /// <param name="newPatient">The new patient.</param>
        public void AddPatient(Patient newPatient)
        {
            this.repo.Add(newPatient);
        }

        /// <summary>
        ///     Searches for the specified patient.
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <returns>The list of patients</returns>
        public IList<Patient> Search(Patient patient)
        {
            return this.repo.Search(patient);
        }

        /// <summary>
        /// Gets the tests.
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <returns>The list of tests</returns>
        public IList<LabTest> GetTests(Patient patient, int appointmentID)
        {
            return this.repo.GetTests(patient, appointmentID);
        }

        /// <summary>
        /// Adds the lab test.
        /// </summary>
        /// <param name="test">The test.</param>
        public void AddLabTest(LabTest test)
        {
            this.repo.AddLabTest(test);
        }

        /// <summary>
        /// Updates the test result.
        /// </summary>
        /// <param name="test">The test.</param>
        public void UpdateTestResult(LabTest test)
        {
            this.repo.UpdateTestResult(test);
        }

        #endregion
    }
}