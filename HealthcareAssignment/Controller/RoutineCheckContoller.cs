﻿using System;
using System.Collections.Generic;
using HealthcareAssignment.DAL.Repositories;
using HealthcareAssignment.Model;

namespace HealthcareAssignment.Controller
{
    internal class RoutineCheckContoller
    {
        #region Data members

        private readonly RoutineCheckRepository repo;

        #endregion

        #region Constructors

        public RoutineCheckContoller()
        {
            this.repo = new RoutineCheckRepository();
        }

        #endregion

        #region Methods

        public Dictionary<DateTime, int> GetAppointMentDates(int patientID)
        {
            return this.repo.GetAppointmentDates(patientID);
        }

        public List<Nurse> GetNurses()
        {
            return this.repo.GetNurses();
        }

        public void Add(RoutineCheck check)
        {
            this.repo.Add(check);
        }

        #endregion
    }
}