﻿using System.Collections.Generic;
using HealthcareAssignment.DAL.Repositories;
using HealthcareAssignment.Model;

namespace HealthcareAssignment.Controller
{
    internal class AppointmentController
    {
        #region Data members

        private readonly AppointmentRepository repo;

        #endregion

        #region Constructors

        public AppointmentController()
        {
            this.repo = new AppointmentRepository();
        }

        #endregion

        #region Methods

        public List<Doctor> GetDoctorIDs()
        {
            return this.repo.GetDoctorIDs();
        }

        public void Add(Appointment apt)
        {
            this.repo.Add(apt);
        }

        #endregion
    }
}