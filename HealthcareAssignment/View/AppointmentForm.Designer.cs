﻿namespace HealthcareAssignment.View
{
    partial class AppointmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.appointmentLabel = new System.Windows.Forms.Label();
            this.doctorLabel = new System.Windows.Forms.Label();
            this.doctorsComboBox = new System.Windows.Forms.ComboBox();
            this.timeLabel = new System.Windows.Forms.Label();
            this.apoointmentTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.reasonTextBox = new System.Windows.Forms.TextBox();
            this.scheduleButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // appointmentLabel
            // 
            this.appointmentLabel.AutoSize = true;
            this.appointmentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appointmentLabel.Location = new System.Drawing.Point(104, 33);
            this.appointmentLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.appointmentLabel.Name = "appointmentLabel";
            this.appointmentLabel.Size = new System.Drawing.Size(271, 24);
            this.appointmentLabel.TabIndex = 14;
            this.appointmentLabel.Text = "Schedule New Appointment";
            // 
            // doctorLabel
            // 
            this.doctorLabel.AutoSize = true;
            this.doctorLabel.Location = new System.Drawing.Point(96, 86);
            this.doctorLabel.Name = "doctorLabel";
            this.doctorLabel.Size = new System.Drawing.Size(42, 13);
            this.doctorLabel.TabIndex = 15;
            this.doctorLabel.Text = "Doctor:";
            // 
            // doctorsComboBox
            // 
            this.doctorsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.doctorsComboBox.FormattingEnabled = true;
            this.doctorsComboBox.Location = new System.Drawing.Point(144, 83);
            this.doctorsComboBox.Name = "doctorsComboBox";
            this.doctorsComboBox.Size = new System.Drawing.Size(121, 21);
            this.doctorsComboBox.TabIndex = 16;
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Location = new System.Drawing.Point(105, 127);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(33, 13);
            this.timeLabel.TabIndex = 18;
            this.timeLabel.Text = "Time:";
            // 
            // apoointmentTimePicker
            // 
            this.apoointmentTimePicker.Location = new System.Drawing.Point(144, 127);
            this.apoointmentTimePicker.Margin = new System.Windows.Forms.Padding(2);
            this.apoointmentTimePicker.Name = "apoointmentTimePicker";
            this.apoointmentTimePicker.Size = new System.Drawing.Size(203, 20);
            this.apoointmentTimePicker.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(91, 172);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Reason:";
            // 
            // reasonTextBox
            // 
            this.reasonTextBox.Location = new System.Drawing.Point(145, 172);
            this.reasonTextBox.Multiline = true;
            this.reasonTextBox.Name = "reasonTextBox";
            this.reasonTextBox.Size = new System.Drawing.Size(202, 69);
            this.reasonTextBox.TabIndex = 21;
            // 
            // scheduleButton
            // 
            this.scheduleButton.Location = new System.Drawing.Point(200, 265);
            this.scheduleButton.Name = "scheduleButton";
            this.scheduleButton.Size = new System.Drawing.Size(75, 23);
            this.scheduleButton.TabIndex = 22;
            this.scheduleButton.Text = "Schedule";
            this.scheduleButton.UseVisualStyleBackColor = true;
            this.scheduleButton.Click += new System.EventHandler(this.scheduleButton_Click);
            // 
            // AppointmentForm
            // 
            this.AcceptButton = this.scheduleButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 310);
            this.Controls.Add(this.scheduleButton);
            this.Controls.Add(this.reasonTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.apoointmentTimePicker);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.doctorsComboBox);
            this.Controls.Add(this.doctorLabel);
            this.Controls.Add(this.appointmentLabel);
            this.Name = "AppointmentForm";
            this.Text = "AppointmentForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label appointmentLabel;
        private System.Windows.Forms.Label doctorLabel;
        private System.Windows.Forms.ComboBox doctorsComboBox;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.DateTimePicker apoointmentTimePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox reasonTextBox;
        private System.Windows.Forms.Button scheduleButton;
    }
}