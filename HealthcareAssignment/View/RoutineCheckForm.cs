﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using HealthcareAssignment.Controller;
using HealthcareAssignment.Model;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.View
{
    public partial class RoutineCheckForm : Form
    {
        #region Data members

        private readonly RoutineCheckContoller routineCheckContoller;
        private string errorMessage = "";
        private readonly int patientID;
        private Dictionary<DateTime, int> dates;

        #endregion

        #region Constructors

        public RoutineCheckForm(int patientID)
        {
            this.InitializeComponent();
            this.routineCheckContoller = new RoutineCheckContoller();
            this.patientID = patientID;
            this.populateComboBoxValues();
        }

        #endregion

        #region Methods

        private void populateComboBoxValues()
        {
            this.dates = this.routineCheckContoller.GetAppointMentDates(this.patientID);
            foreach (var date in this.dates)
            {
                this.appointmentIDCombobox.Items.Add(date.Key);
            }
            foreach (var nurse in this.routineCheckContoller.GetNurses())
            {
                this.nurseNamesCombobox.Items.Add(nurse);
            }
        }

        private void submitResultsButton_Click(object sender, EventArgs e)
        {
            if (!this.containsValidInput())
            {
                MessageBox.Show(this.errorMessage);
            }
            else
            {
                try
                {
                    var check = new RoutineCheck();
                    check.AppointmentID = Convert.ToInt32(this.dates[(DateTime) this.appointmentIDCombobox.SelectedItem]);
                    var selectedNurse = (Nurse) this.nurseNamesCombobox.SelectedItem;
                    check.NurseId = selectedNurse.NurseId;
                    check.BodyTemperature = Convert.ToDecimal(this.bodyTempTextBox.Text);
                    check.Systolic = Convert.ToInt32(this.systolicTextBox.Text);
                    check.Diastolic = Convert.ToInt32(this.diastolicTextBox.Text);
                    check.Pulse = Convert.ToInt32(this.pulseTextBox.Text);
                    this.routineCheckContoller.Add(check);
                    MessageBox.Show("The checkup has successfully been completed.");
                    this.resetToDefaultValues();
                }
                catch (MySqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void resetToDefaultValues()
        {
            this.appointmentIDCombobox.Text = string.Empty;
            this.nurseNamesCombobox.Text = string.Empty;
            this.bodyTempTextBox.Text = string.Empty;
            this.systolicTextBox.Text = string.Empty;
            this.diastolicTextBox.Text = string.Empty;
            this.pulseTextBox.Text = string.Empty;
        }

        private bool containsValidInput()
        {
            this.errorMessage = "";
            var validInput = true;
            if (this.appointmentIDCombobox.Text == string.Empty)
            {
                this.errorMessage += "*Must enter an existing Appointment ID." + Environment.NewLine;
                validInput = false;
            }
            if (this.nurseNamesCombobox.Text == string.Empty)
            {
                this.errorMessage += "*Must enter an existing Nurse ID." + Environment.NewLine;
                validInput = false;
            }
            if (!Regex.IsMatch(this.bodyTempTextBox.Text, "^\\d{1,5}(.)\\d{1,2}$"))
            {
                this.errorMessage += "*Body temp does not meet correct format." + Environment.NewLine;
                validInput = false;
            }
            if (!Regex.IsMatch(this.systolicTextBox.Text, "^\\d{1,11}$"))
            {
                this.errorMessage += "*Systolic reading should only contain digits.  " +
                                     Environment.NewLine;
                validInput = false;
            }
            if (!Regex.IsMatch(this.diastolicTextBox.Text, "^\\d{1,11}$"))
            {
                this.errorMessage += "*Diastolic reading should only contain digits.  " +
                                     Environment.NewLine;
                validInput = false;
            }
            if (!Regex.IsMatch(this.pulseTextBox.Text, "^\\d{1,11}$"))
            {
                this.errorMessage += "*Pulse reading should only contain digits.  " +
                                     Environment.NewLine;
                validInput = false;
            }
            return validInput;
        }

        #endregion
    }
}