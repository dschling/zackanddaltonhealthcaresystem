﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using HealthcareAssignment.Controller;
using HealthcareAssignment.Model;
using HealthcareAssignment.Properties;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.View
{
    public partial class RegistryForm : Form
    {
        #region Data members

        private string errorMessage = "";
        private readonly PatientRepositoryController patientRepo;

        #endregion

        #region Constructors

        public RegistryForm(PatientRepositoryController patientRepo)
        {
            this.InitializeComponent();
            this.maleRadioButton.Checked = true;
            this.patientRepo = patientRepo;
            this.populateStateDropdown();
        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            if (!this.containsValidInput())
            {
                MessageBox.Show(this.errorMessage);
            }
            else
            {
                try
                {
                    var newPatient = new Patient(this.firstNameTextbox.Text, this.lastNameTextbox.Text);
                    newPatient.Birthdate = Convert.ToDateTime(this.birthDateChooser.Text);
                    newPatient.Gender = this.GetGenderFromRadioButtons();
                    newPatient.Street = this.streetTextbox.Text;
                    newPatient.City = this.cityTextbox.Text;
                    newPatient.State = this.stateComboBox.Text;
                    newPatient.Zip = Convert.ToInt32(this.zipTextBox.Text);
                    long phoneNumber = 0;
                    long.TryParse(this.phoneNumberTextbox.Text, out phoneNumber);
                    newPatient.PhoneNumber = phoneNumber;

                    this.patientRepo.AddPatient(newPatient);
                    MessageBox.Show("Patient " + newPatient.FirstName + " " + newPatient.LastName +
                                    " has been added to the database");
                    this.returnToDefaultValues();
                    Close();
                }
                catch (MySqlException x)
                {
                    MessageBox.Show(x.Message);
                }
            }
        }

        private void returnToDefaultValues()
        {
            this.firstNameTextbox.Clear();
            this.lastNameTextbox.Clear();
            this.birthDateChooser.Text = DateTime.Today.ToShortDateString();
            this.maleRadioButton.Checked = true;
            this.femaleRadioButton.Checked = false;
            this.streetTextbox.Clear();
            this.phoneNumberTextbox.Clear();
        }

        private bool containsValidInput()
        {
            this.errorMessage = "";
            var validInput = true;
            if (this.firstNameTextbox.Text.Equals(string.Empty))
            {
                this.errorMessage += "*Must enter Patient's first name." + Environment.NewLine;
                validInput = false;
            }
            if (this.lastNameTextbox.Text.Equals(string.Empty))
            {
                this.errorMessage += "*Must enter Patient's last name." + Environment.NewLine;
                validInput = false;
            }
            if (this.streetTextbox.Text.Equals(string.Empty))
            {
                this.errorMessage += "*Must enter a street." + Environment.NewLine;
                validInput = false;
            }
            if (this.cityTextbox.Text.Equals(string.Empty))
            {
                this.errorMessage += "*Must enter a city." + Environment.NewLine;
                validInput = false;
            }
            if (this.stateComboBox.Text.Equals(string.Empty))
            {
                this.errorMessage += "*Must enter a state." + Environment.NewLine;
                validInput = false;
            }
            if (!Regex.IsMatch(this.zipTextBox.Text, "^\\d{1,11}$"))
            {
                this.errorMessage += "*Zip must be a series of digits." + Environment.NewLine;
                validInput = false;
            }
            if (!Regex.IsMatch(this.phoneNumberTextbox.Text, "^[0-9]{10}$"))
            {
                this.errorMessage += "*Patient's phone number must be 10 digits. No spaces or special characters." +
                                     Environment.NewLine;
                validInput = false;
            }
            return validInput;
        }

        private void populateStateDropdown()
        {
            var states = Resources.States.Split(',');

            foreach (var state in states)
            {
                this.stateComboBox.Items.Add(state);
            }
        }

        /// <summary>
        ///     Gets the gender from radio buttons.
        /// </summary>
        /// <returns>M or F as a char</returns>
        public char GetGenderFromRadioButtons()
        {
            var gender = '\0';
            if (this.maleRadioButton.Checked)
            {
                gender = 'M';
            }
            else if (this.femaleRadioButton.Checked)
            {
                gender = 'F';
            }
            return gender;
        }

        private void maleRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this.maleRadioButton.Checked)
            {
                this.femaleRadioButton.Checked = false;
            }
            else
            {
                this.femaleRadioButton.Checked = true;
            }
        }

        private void femaleRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this.femaleRadioButton.Checked)
            {
                this.maleRadioButton.Checked = false;
            }
            else
            {
                this.maleRadioButton.Checked = true;
            }
        }
    }
}