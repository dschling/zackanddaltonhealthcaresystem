﻿namespace HealthcareAssignment.View
{
    partial class VisitInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.testsGridView = new System.Windows.Forms.DataGridView();
            this.testsLabel = new System.Windows.Forms.Label();
            this.orderTestButton = new System.Windows.Forms.Button();
            this.orderTestGroupBox = new System.Windows.Forms.GroupBox();
            this.testCodeCombobox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.diagnosisGroupbox = new System.Windows.Forms.GroupBox();
            this.recordDiagnosisTextbox = new System.Windows.Forms.TextBox();
            this.recordDiagnosisButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.appointmentComboBox = new System.Windows.Forms.ComboBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.diagnosisTextBox = new System.Windows.Forms.TextBox();
            this.diagnosesLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.testsGridView)).BeginInit();
            this.orderTestGroupBox.SuspendLayout();
            this.diagnosisGroupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // testsGridView
            // 
            this.testsGridView.AllowUserToAddRows = false;
            this.testsGridView.AllowUserToDeleteRows = false;
            this.testsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.testsGridView.Location = new System.Drawing.Point(34, 129);
            this.testsGridView.MultiSelect = false;
            this.testsGridView.Name = "testsGridView";
            this.testsGridView.RowHeadersVisible = false;
            this.testsGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.testsGridView.Size = new System.Drawing.Size(397, 192);
            this.testsGridView.TabIndex = 0;
            this.testsGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.testsGridView_CellValueChanged);
            // 
            // testsLabel
            // 
            this.testsLabel.AutoSize = true;
            this.testsLabel.Location = new System.Drawing.Point(32, 112);
            this.testsLabel.Name = "testsLabel";
            this.testsLabel.Size = new System.Drawing.Size(33, 13);
            this.testsLabel.TabIndex = 1;
            this.testsLabel.Text = "Tests";
            // 
            // orderTestButton
            // 
            this.orderTestButton.Location = new System.Drawing.Point(146, 77);
            this.orderTestButton.Name = "orderTestButton";
            this.orderTestButton.Size = new System.Drawing.Size(75, 23);
            this.orderTestButton.TabIndex = 4;
            this.orderTestButton.Text = "Order Test";
            this.orderTestButton.UseVisualStyleBackColor = true;
            this.orderTestButton.Click += new System.EventHandler(this.orderTestButton_Click);
            // 
            // orderTestGroupBox
            // 
            this.orderTestGroupBox.Controls.Add(this.testCodeCombobox);
            this.orderTestGroupBox.Controls.Add(this.label2);
            this.orderTestGroupBox.Controls.Add(this.label1);
            this.orderTestGroupBox.Controls.Add(this.orderTestButton);
            this.orderTestGroupBox.Location = new System.Drawing.Point(34, 393);
            this.orderTestGroupBox.Name = "orderTestGroupBox";
            this.orderTestGroupBox.Size = new System.Drawing.Size(241, 111);
            this.orderTestGroupBox.TabIndex = 6;
            this.orderTestGroupBox.TabStop = false;
            this.orderTestGroupBox.Text = "Order Test";
            // 
            // testCodeCombobox
            // 
            this.testCodeCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.testCodeCombobox.FormattingEnabled = true;
            this.testCodeCombobox.Location = new System.Drawing.Point(79, 31);
            this.testCodeCombobox.Name = "testCodeCombobox";
            this.testCodeCombobox.Size = new System.Drawing.Size(121, 21);
            this.testCodeCombobox.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Test Code";
            // 
            // diagnosisGroupbox
            // 
            this.diagnosisGroupbox.Controls.Add(this.recordDiagnosisTextbox);
            this.diagnosisGroupbox.Controls.Add(this.recordDiagnosisButton);
            this.diagnosisGroupbox.Location = new System.Drawing.Point(559, 393);
            this.diagnosisGroupbox.Name = "diagnosisGroupbox";
            this.diagnosisGroupbox.Size = new System.Drawing.Size(241, 167);
            this.diagnosisGroupbox.TabIndex = 7;
            this.diagnosisGroupbox.TabStop = false;
            this.diagnosisGroupbox.Text = "Record Diagnoses";
            // 
            // recordDiagnosisTextbox
            // 
            this.recordDiagnosisTextbox.Location = new System.Drawing.Point(22, 31);
            this.recordDiagnosisTextbox.Multiline = true;
            this.recordDiagnosisTextbox.Name = "recordDiagnosisTextbox";
            this.recordDiagnosisTextbox.Size = new System.Drawing.Size(203, 87);
            this.recordDiagnosisTextbox.TabIndex = 5;
            // 
            // recordDiagnosisButton
            // 
            this.recordDiagnosisButton.Location = new System.Drawing.Point(160, 138);
            this.recordDiagnosisButton.Name = "recordDiagnosisButton";
            this.recordDiagnosisButton.Size = new System.Drawing.Size(75, 23);
            this.recordDiagnosisButton.TabIndex = 4;
            this.recordDiagnosisButton.Text = "Record";
            this.recordDiagnosisButton.UseVisualStyleBackColor = true;
            this.recordDiagnosisButton.Click += new System.EventHandler(this.recordDiagnosisButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Appointment";
            // 
            // appointmentComboBox
            // 
            this.appointmentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.appointmentComboBox.FormattingEnabled = true;
            this.appointmentComboBox.Location = new System.Drawing.Point(104, 66);
            this.appointmentComboBox.Name = "appointmentComboBox";
            this.appointmentComboBox.Size = new System.Drawing.Size(121, 21);
            this.appointmentComboBox.TabIndex = 9;
            // 
            // titleLabel
            // 
            this.titleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.titleLabel.Location = new System.Drawing.Point(31, 30);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(221, 24);
            this.titleLabel.TabIndex = 10;
            this.titleLabel.Text = "Select an Appointment";
            // 
            // diagnosisTextBox
            // 
            this.diagnosisTextBox.Location = new System.Drawing.Point(559, 129);
            this.diagnosisTextBox.Multiline = true;
            this.diagnosisTextBox.Name = "diagnosisTextBox";
            this.diagnosisTextBox.Size = new System.Drawing.Size(303, 192);
            this.diagnosisTextBox.TabIndex = 11;
            // 
            // diagnosesLabel
            // 
            this.diagnosesLabel.AutoSize = true;
            this.diagnosesLabel.Location = new System.Drawing.Point(556, 113);
            this.diagnosesLabel.Name = "diagnosesLabel";
            this.diagnosesLabel.Size = new System.Drawing.Size(57, 13);
            this.diagnosesLabel.TabIndex = 12;
            this.diagnosesLabel.Text = "Diagnoses";
            // 
            // VisitInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 633);
            this.Controls.Add(this.diagnosesLabel);
            this.Controls.Add(this.diagnosisTextBox);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.appointmentComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.diagnosisGroupbox);
            this.Controls.Add(this.orderTestGroupBox);
            this.Controls.Add(this.testsLabel);
            this.Controls.Add(this.testsGridView);
            this.Name = "VisitInfoForm";
            this.Text = "VisitInfoForm";
            ((System.ComponentModel.ISupportInitialize)(this.testsGridView)).EndInit();
            this.orderTestGroupBox.ResumeLayout(false);
            this.orderTestGroupBox.PerformLayout();
            this.diagnosisGroupbox.ResumeLayout(false);
            this.diagnosisGroupbox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView testsGridView;
        private System.Windows.Forms.Label testsLabel;
        private System.Windows.Forms.Button orderTestButton;
        private System.Windows.Forms.GroupBox orderTestGroupBox;
        private System.Windows.Forms.ComboBox testCodeCombobox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox diagnosisGroupbox;
        private System.Windows.Forms.TextBox recordDiagnosisTextbox;
        private System.Windows.Forms.Button recordDiagnosisButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox appointmentComboBox;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.TextBox diagnosisTextBox;
        private System.Windows.Forms.Label diagnosesLabel;
    }
}