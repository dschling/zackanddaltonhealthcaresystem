﻿namespace HealthcareAssignment.View
{
    partial class RoutineCheckForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.routineCheckLabel = new System.Windows.Forms.Label();
            this.appointmentIDCombobox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nurseNamesCombobox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bodyTempTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.systolicTextBox = new System.Windows.Forms.TextBox();
            this.diastolicTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pulseTextBox = new System.Windows.Forms.TextBox();
            this.submitResultsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // routineCheckLabel
            // 
            this.routineCheckLabel.AutoSize = true;
            this.routineCheckLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.routineCheckLabel.Location = new System.Drawing.Point(111, 41);
            this.routineCheckLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.routineCheckLabel.Name = "routineCheckLabel";
            this.routineCheckLabel.Size = new System.Drawing.Size(223, 24);
            this.routineCheckLabel.TabIndex = 15;
            this.routineCheckLabel.Text = "Enter Checkup Results";
            // 
            // appointmentIDCombobox
            // 
            this.appointmentIDCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.appointmentIDCombobox.FormattingEnabled = true;
            this.appointmentIDCombobox.Location = new System.Drawing.Point(198, 82);
            this.appointmentIDCombobox.Margin = new System.Windows.Forms.Padding(2);
            this.appointmentIDCombobox.Name = "appointmentIDCombobox";
            this.appointmentIDCombobox.Size = new System.Drawing.Size(136, 21);
            this.appointmentIDCombobox.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 85);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Appointment Dates:";
            // 
            // nurseNamesCombobox
            // 
            this.nurseNamesCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.nurseNamesCombobox.FormattingEnabled = true;
            this.nurseNamesCombobox.Location = new System.Drawing.Point(198, 132);
            this.nurseNamesCombobox.Margin = new System.Windows.Forms.Padding(2);
            this.nurseNamesCombobox.Name = "nurseNamesCombobox";
            this.nurseNamesCombobox.Size = new System.Drawing.Size(136, 21);
            this.nurseNamesCombobox.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(155, 135);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Nurse:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(96, 178);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Body Temperature:";
            // 
            // bodyTempTextBox
            // 
            this.bodyTempTextBox.Location = new System.Drawing.Point(198, 176);
            this.bodyTempTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.bodyTempTextBox.Name = "bodyTempTextBox";
            this.bodyTempTextBox.Size = new System.Drawing.Size(76, 20);
            this.bodyTempTextBox.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(102, 266);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Diastolic Reading:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(106, 223);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Systolic Reading:";
            // 
            // systolicTextBox
            // 
            this.systolicTextBox.Location = new System.Drawing.Point(198, 221);
            this.systolicTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.systolicTextBox.Name = "systolicTextBox";
            this.systolicTextBox.Size = new System.Drawing.Size(76, 20);
            this.systolicTextBox.TabIndex = 24;
            // 
            // diastolicTextBox
            // 
            this.diastolicTextBox.Location = new System.Drawing.Point(198, 263);
            this.diastolicTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.diastolicTextBox.Name = "diastolicTextBox";
            this.diastolicTextBox.Size = new System.Drawing.Size(76, 20);
            this.diastolicTextBox.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(116, 308);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Pulse Reading:";
            // 
            // pulseTextBox
            // 
            this.pulseTextBox.Location = new System.Drawing.Point(198, 306);
            this.pulseTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.pulseTextBox.Name = "pulseTextBox";
            this.pulseTextBox.Size = new System.Drawing.Size(76, 20);
            this.pulseTextBox.TabIndex = 27;
            // 
            // submitResultsButton
            // 
            this.submitResultsButton.Location = new System.Drawing.Point(159, 355);
            this.submitResultsButton.Margin = new System.Windows.Forms.Padding(2);
            this.submitResultsButton.Name = "submitResultsButton";
            this.submitResultsButton.Size = new System.Drawing.Size(92, 25);
            this.submitResultsButton.TabIndex = 28;
            this.submitResultsButton.Text = "Submit Results";
            this.submitResultsButton.UseVisualStyleBackColor = true;
            this.submitResultsButton.Click += new System.EventHandler(this.submitResultsButton_Click);
            // 
            // RoutineCheckForm
            // 
            this.AcceptButton = this.submitResultsButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 429);
            this.Controls.Add(this.submitResultsButton);
            this.Controls.Add(this.pulseTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.diastolicTextBox);
            this.Controls.Add(this.systolicTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.bodyTempTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nurseNamesCombobox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.appointmentIDCombobox);
            this.Controls.Add(this.routineCheckLabel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "RoutineCheckForm";
            this.Text = "RoutineCheckForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label routineCheckLabel;
        private System.Windows.Forms.ComboBox appointmentIDCombobox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox nurseNamesCombobox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox bodyTempTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox systolicTextBox;
        private System.Windows.Forms.TextBox diastolicTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox pulseTextBox;
        private System.Windows.Forms.Button submitResultsButton;
    }
}