﻿using System;
using System.Windows.Forms;
using HealthcareAssignment.Controller;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.View
{
    public partial class AdminQueryForm : Form
    {
        #region Data members

        private readonly BindingSource bindingSource = new BindingSource();
        private readonly AdminQueryController adminQueryController;

        #endregion

        #region Constructors

        public AdminQueryForm()
        {
            this.InitializeComponent();
            this.adminQueryController = new AdminQueryController();
        }

        #endregion

        private void executeButton_Click(object sender, EventArgs e)
        {
            this.initializeDataGridView();
        }

        private void initializeDataGridView()
        {
            this.queryDataGrid.AutoGenerateColumns = true;
            try
            {
                this.bindingSource.DataSource =
                    this.adminQueryController.GetDataFromQuery(this.queryTextbox.Text.ToUpper());
                this.queryDataGrid.DataSource = this.bindingSource;
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }

            this.queryDataGrid.AutoSizeRowsMode =
                DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;

            this.queryDataGrid.BorderStyle = BorderStyle.Fixed3D;

            this.queryDataGrid.EditMode = DataGridViewEditMode.EditOnEnter;
            this.queryDataGrid.RowHeadersVisible = false;
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            this.queryDataGrid.AutoGenerateColumns = true;

            try
            {
                this.bindingSource.DataSource =
                    this.adminQueryController.GetDataInTimePeriod(this.startDatePicker.Value, this.dateTimePicker1.Value);
                this.queryDataGrid.DataSource = this.bindingSource;
            }
            catch (MySqlException x)
            {
                MessageBox.Show(x.Message);
            }

            this.queryDataGrid.AutoSizeRowsMode =
                DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;

            this.queryDataGrid.BorderStyle = BorderStyle.Fixed3D;

            this.queryDataGrid.EditMode = DataGridViewEditMode.EditOnEnter;
            this.queryDataGrid.RowHeadersVisible = false;
        }
    }
}