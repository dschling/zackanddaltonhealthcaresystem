﻿using System.Windows.Forms;

namespace HealthcareAssignment.View
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titleLabel = new System.Windows.Forms.Label();
            this.firstNameTextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.birthDateChooser = new System.Windows.Forms.DateTimePicker();
            this.searchButton = new System.Windows.Forms.Button();
            this.newPatientButton = new System.Windows.Forms.Button();
            this.searchResultsDataGridView = new System.Windows.Forms.DataGridView();
            this.userMenuStrip = new System.Windows.Forms.MenuStrip();
            this.userNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.appointmentButton = new System.Windows.Forms.Button();
            this.routineCheckButton = new System.Windows.Forms.Button();
            this.visitInfoButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.searchResultsDataGridView)).BeginInit();
            this.userMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.titleLabel.Location = new System.Drawing.Point(107, 63);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(203, 24);
            this.titleLabel.TabIndex = 0;
            this.titleLabel.Text = "Search For A Patient";
            // 
            // firstNameTextbox
            // 
            this.firstNameTextbox.Location = new System.Drawing.Point(155, 112);
            this.firstNameTextbox.Margin = new System.Windows.Forms.Padding(2);
            this.firstNameTextbox.Name = "firstNameTextbox";
            this.firstNameTextbox.Size = new System.Drawing.Size(151, 20);
            this.firstNameTextbox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 115);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Patient\'s First Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 154);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Patient\'s Last Name:";
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(156, 154);
            this.lastNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(151, 20);
            this.lastNameTextBox.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(83, 198);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Date of Birth:";
            // 
            // birthDateChooser
            // 
            this.birthDateChooser.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.birthDateChooser.Location = new System.Drawing.Point(155, 198);
            this.birthDateChooser.Margin = new System.Windows.Forms.Padding(2);
            this.birthDateChooser.Name = "birthDateChooser";
            this.birthDateChooser.Size = new System.Drawing.Size(151, 20);
            this.birthDateChooser.TabIndex = 12;
            this.birthDateChooser.Value = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(192, 247);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 13;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // newPatientButton
            // 
            this.newPatientButton.Location = new System.Drawing.Point(32, 310);
            this.newPatientButton.Name = "newPatientButton";
            this.newPatientButton.Size = new System.Drawing.Size(104, 23);
            this.newPatientButton.TabIndex = 14;
            this.newPatientButton.Text = "Add New Patient";
            this.newPatientButton.UseVisualStyleBackColor = true;
            this.newPatientButton.Click += new System.EventHandler(this.newPatientButtonClick);
            // 
            // searchResultsDataGridView
            // 
            this.searchResultsDataGridView.AllowUserToAddRows = false;
            this.searchResultsDataGridView.AllowUserToDeleteRows = false;
            this.searchResultsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.searchResultsDataGridView.Location = new System.Drawing.Point(446, 72);
            this.searchResultsDataGridView.MultiSelect = false;
            this.searchResultsDataGridView.Name = "searchResultsDataGridView";
            this.searchResultsDataGridView.RowHeadersVisible = false;
            this.searchResultsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.searchResultsDataGridView.Size = new System.Drawing.Size(419, 304);
            this.searchResultsDataGridView.TabIndex = 15;
            this.searchResultsDataGridView.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.rowSelected);
            // 
            // userMenuStrip
            // 
            this.userMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.userMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userNameToolStripMenuItem});
            this.userMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.userMenuStrip.Name = "userMenuStrip";
            this.userMenuStrip.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userMenuStrip.Size = new System.Drawing.Size(889, 24);
            this.userMenuStrip.TabIndex = 16;
            this.userMenuStrip.Text = "menuStrip1";
            // 
            // userNameToolStripMenuItem
            // 
            this.userNameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logoutToolStripMenuItem});
            this.userNameToolStripMenuItem.Name = "userNameToolStripMenuItem";
            this.userNameToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.userNameToolStripMenuItem.Text = "User name";
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.logoutToolStripMenuItem.Text = "Logout";
            // 
            // appointmentButton
            // 
            this.appointmentButton.Enabled = false;
            this.appointmentButton.Location = new System.Drawing.Point(155, 310);
            this.appointmentButton.Margin = new System.Windows.Forms.Padding(2);
            this.appointmentButton.Name = "appointmentButton";
            this.appointmentButton.Size = new System.Drawing.Size(106, 23);
            this.appointmentButton.TabIndex = 17;
            this.appointmentButton.Text = "Add Appointment";
            this.appointmentButton.UseVisualStyleBackColor = true;
            this.appointmentButton.Click += new System.EventHandler(this.appointmentButton_Click);
            // 
            // routineCheckButton
            // 
            this.routineCheckButton.Enabled = false;
            this.routineCheckButton.Location = new System.Drawing.Point(280, 310);
            this.routineCheckButton.Margin = new System.Windows.Forms.Padding(2);
            this.routineCheckButton.Name = "routineCheckButton";
            this.routineCheckButton.Size = new System.Drawing.Size(100, 23);
            this.routineCheckButton.TabIndex = 18;
            this.routineCheckButton.Text = "Record Checkup";
            this.routineCheckButton.UseVisualStyleBackColor = true;
            this.routineCheckButton.Click += new System.EventHandler(this.routineCheckButton_Click);
            // 
            // visitInfoButton
            // 
            this.visitInfoButton.Enabled = false;
            this.visitInfoButton.Location = new System.Drawing.Point(602, 382);
            this.visitInfoButton.Name = "visitInfoButton";
            this.visitInfoButton.Size = new System.Drawing.Size(99, 23);
            this.visitInfoButton.TabIndex = 19;
            this.visitInfoButton.Text = "View Visit Info";
            this.visitInfoButton.UseVisualStyleBackColor = true;
            this.visitInfoButton.Click += new System.EventHandler(this.visitInfoButton_Click);
            // 
            // SearchForm
            // 
            this.AcceptButton = this.searchButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 459);
            this.Controls.Add(this.visitInfoButton);
            this.Controls.Add(this.routineCheckButton);
            this.Controls.Add(this.appointmentButton);
            this.Controls.Add(this.searchResultsDataGridView);
            this.Controls.Add(this.newPatientButton);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.birthDateChooser);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lastNameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.firstNameTextbox);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.userMenuStrip);
            this.MainMenuStrip = this.userMenuStrip;
            this.Name = "SearchForm";
            this.Text = "SearchForm";
            ((System.ComponentModel.ISupportInitialize)(this.searchResultsDataGridView)).EndInit();
            this.userMenuStrip.ResumeLayout(false);
            this.userMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.TextBox firstNameTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker birthDateChooser;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button newPatientButton;
        private System.Windows.Forms.DataGridView searchResultsDataGridView;
        private System.Windows.Forms.MenuStrip userMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem userNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.Button appointmentButton;
        private System.Windows.Forms.Button routineCheckButton;
        private Button visitInfoButton;
    }
}