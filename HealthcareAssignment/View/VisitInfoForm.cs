﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using HealthcareAssignment.Controller;
using HealthcareAssignment.Model;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.View
{
    public partial class VisitInfoForm : Form
    {
        #region Data members

        private readonly int patientId;
        private readonly PatientRepositoryController controller;
        private readonly DiagnosisController diagnosisController;
        private Dictionary<DateTime, int> dates;

        #endregion

        #region Constructors

        public VisitInfoForm(int patientId)
        {
            this.patientId = patientId;
            this.InitializeComponent();
            this.populateLabTestCodes();
            this.populateAppointmentsDropBox();
            this.controller = new PatientRepositoryController();
            this.diagnosisController = new DiagnosisController();
            this.orderTestGroupBox.Enabled = false;
            this.diagnosisGroupbox.Enabled = false;
            this.testsGridView.Columns.Add("appointmentID", "Appointment ID");
            this.testsGridView.Columns.Add("testCode", "Test Code");
            this.testsGridView.Columns.Add("result", "Result");
            this.testsGridView.Columns.Add("datePerformed", "Date Performed");
            this.appointmentComboBox.SelectedIndexChanged += this.showAppointmentInfo;
        }

        #endregion

        private void showAppointmentInfo(object sender, EventArgs e)
        {
            this.diagnosisGroupbox.Enabled = true;
            this.orderTestGroupBox.Enabled = true;

            this.populateTestDataGrid();
            var diagnosis =
                this.diagnosisController.GetDiagnosis(this.dates[(DateTime) this.appointmentComboBox.SelectedItem]);

            this.showCurrentDiagnosis(diagnosis);
        }

        private void populateTestDataGrid()
        {
            this.testsGridView.Rows.Clear();
            var tests = this.controller.GetTests(new Patient {PatientId = this.patientId}, this.dates[(DateTime)this.appointmentComboBox.SelectedItem]);
            foreach (var test in tests)
            {
                var date = string.Empty;
                if (!test.TestDate.ToShortDateString().Equals("1/1/0001"))
                {
                    date = test.TestDate.ToShortDateString();
                }
                string[] row = {test.AppointmentId.ToString(), test.TestCode, test.Result, date};
                this.testsGridView.Rows.Add(row);
            }
            var appointmentColumn = this.testsGridView.Columns["appointmentID"];
            if (appointmentColumn != null)
            {
                appointmentColumn.ReadOnly = true;
            }

            var testCodeColumn = this.testsGridView.Columns["testCode"];
            if (testCodeColumn != null)
            {
                testCodeColumn.ReadOnly = true;
            }
            var testDateColumn = this.testsGridView.Columns["datePerformed"];
            if (testDateColumn != null)
            {
                testDateColumn.ReadOnly = true;
            }
        }

        private void showCurrentDiagnosis(Diagnosis diagnosis)
        {
            var currentDiagnosis = "Initial Diagnosis: ";
            if (string.IsNullOrEmpty(diagnosis.InitialDiagnosis))
            {
                currentDiagnosis += "There is currently no intial diagnosis." + Environment.NewLine;
            }
            else
            {
                currentDiagnosis += diagnosis.InitialDiagnosis + Environment.NewLine;
            }
            currentDiagnosis += "Final Diagnosis: ";
            if (string.IsNullOrEmpty(diagnosis.FinalDiagnosis))
            {
                currentDiagnosis += "There is currently no final diagnosis.";
            }
            else
            {
                currentDiagnosis += diagnosis.FinalDiagnosis;
                this.orderTestGroupBox.Enabled = false;
            }
            this.diagnosisTextBox.Text = currentDiagnosis;
        }

        private void populateLabTestCodes()
        {
            this.testCodeCombobox.Items.Add("WBC");
            this.testCodeCombobox.Items.Add("LDL");
            this.testCodeCombobox.Items.Add("HEPA");
            this.testCodeCombobox.Items.Add("HEPB");
            this.testCodeCombobox.Items.Add("HEPC");
        }

        private void populateAppointmentsDropBox()
        {
            var controller = new RoutineCheckContoller();
            this.dates = controller.GetAppointMentDates(this.patientId);

            foreach (var date in this.dates)
            {
                this.appointmentComboBox.Items.Add(date.Key);
            }
            if (this.appointmentComboBox.Items.Count == 0)
            {
                this.diagnosisGroupbox.Enabled = false;
                this.orderTestGroupBox.Enabled = false;
            }
        }

        private void recordDiagnosisButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.recordDiagnosisTextbox.Text))
                {
                    MessageBox.Show("Please enter a diagnosis");
                }
                else
                {
                    var diagnosis =
                        this.diagnosisController.GetDiagnosis(
                            this.dates[(DateTime) this.appointmentComboBox.SelectedItem]);
                    if (string.IsNullOrEmpty(diagnosis.InitialDiagnosis))
                    {
                        diagnosis = new Diagnosis {
                            AppointmentId = this.dates[(DateTime) this.appointmentComboBox.SelectedItem],
                            InitialDiagnosis = this.recordDiagnosisTextbox.Text
                        };
                        this.diagnosisController.AddDiagnosis(diagnosis);
                    }
                    else
                    {
                        this.diagnosisController.AddFinalDiagnosis(
                            this.dates[(DateTime) this.appointmentComboBox.SelectedItem],
                            this.recordDiagnosisTextbox.Text);
                        diagnosis =
                            this.diagnosisController.GetDiagnosis(
                                this.dates[(DateTime) this.appointmentComboBox.SelectedItem]);
                        diagnosis.FinalDiagnosis = this.recordDiagnosisTextbox.Text;
                        this.orderTestGroupBox.Enabled = false;
                    }
                    this.showCurrentDiagnosis(diagnosis);
                }
            }
            catch (MySqlException x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void orderTestButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.testCodeCombobox.SelectedItem == null)
                {
                    MessageBox.Show("Please select a lab test code to add");
                }
                else
                {
                    var test = new LabTest(this.dates[(DateTime) this.appointmentComboBox.SelectedItem],
                        this.testCodeCombobox.Text);
                    this.controller.AddLabTest(test);
                    this.populateTestDataGrid();
                }
            }
            catch (MySqlException x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void testsGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var result = this.testsGridView.CurrentCell.Value.ToString();

                var dataGridViewRow = this.testsGridView.CurrentRow;
                if (dataGridViewRow != null)
                {
                    var test = new LabTest(this.dates[(DateTime) this.appointmentComboBox.SelectedItem],
                        dataGridViewRow.Cells[1].Value.ToString());
                    test.Result = result;
                    test.TestDate = DateTime.Today;
                    this.controller.UpdateTestResult(test);
                    MessageBox.Show("Test results for the " + dataGridViewRow.Cells[1].Value + " test have been updated");
                    this.populateTestDataGrid();
                }
            }
            catch (MySqlException x)
            {
                MessageBox.Show(x.Message);
            }
        }
    }
}