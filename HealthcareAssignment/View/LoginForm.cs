﻿using System;
using System.Windows.Forms;
using HealthcareAssignment.Controller;
using HealthcareAssignment.Utility;

namespace HealthcareAssignment.View
{
    public partial class LoginForm : Form
    {
        #region Data members

        private readonly LoginValidatorController validator;

        #endregion

        #region Constructors

        public LoginForm()
        {
            this.InitializeComponent();
            AcceptButton = this.loginButton;
            this.passwordTextbox.PasswordChar = '*';
            this.validator = new LoginValidatorController();
        }

        #endregion

        #region Methods

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (this.userIDTextbox.Text.Equals(string.Empty) || this.passwordTextbox.Text.Equals(string.Empty))
            {
                MessageBox.Show("All fields are required to login.");
            }
            else
            {
                var userID = Convert.ToInt32(this.userIDTextbox.Text);
                var password = StringEncrytor.Encrypt(this.passwordTextbox.Text);

                if (!this.validator.IsValidUser(userID, password))
                {
                    MessageBox.Show(
                        "You did not enter valid credentials. Please enter your correct login information to continue.");
                }
                else
                {
                    Hide();
                    var searchForm = new SearchForm();
                    AddOwnedForm(searchForm);
                    searchForm.SetUserOnMenuStrip(this.validator.GetCurrentUserName(userID));

                    if (this.validator.IsAdmin(userID, password))
                    {
                        searchForm.AddSqlWindowMenuItem();
                    }
                    searchForm.Show();
                }
            }
        }

        #endregion
    }
}