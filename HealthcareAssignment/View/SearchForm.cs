﻿using System;
using System.Windows.Forms;
using HealthcareAssignment.Controller;
using HealthcareAssignment.Model;

namespace HealthcareAssignment.View
{
    public partial class SearchForm : Form
    {
        #region Data members

        private readonly PatientRepositoryController patientController;
        private readonly ToolStripMenuItem sqlWindowIdItem;

        #endregion

        #region Constructors

        public SearchForm()
        {
            this.InitializeComponent();
            this.patientController = new PatientRepositoryController();
            this.searchResultsDataGridView.Columns.Add("patientID", "Patient ID");
            this.searchResultsDataGridView.Columns.Add("firstName", "First Name");
            this.searchResultsDataGridView.Columns.Add("lastName", "Last Name");
            this.searchResultsDataGridView.Columns.Add("dob", "Date of Birth");
            this.searchResultsDataGridView.Columns.Add("gender", "Gender");
            this.searchResultsDataGridView.Columns.Add("street", "Street");
            this.searchResultsDataGridView.Columns.Add("city", "City");
            this.searchResultsDataGridView.Columns.Add("state", "State");
            this.searchResultsDataGridView.Columns.Add("zip", "Zip");
            this.searchResultsDataGridView.Columns.Add("phoneNumber", "Phone Number");
            this.sqlWindowIdItem = new ToolStripMenuItem("Open Admin window");
            this.sqlWindowIdItem.Click += this.openSQLWindow;
            FormClosing += this.showConfirmationOnClosing;
        }

        #endregion

        #region Methods

        public void AddSqlWindowMenuItem(){
            
            this.userNameToolStripMenuItem.DropDownItems.Add(this.sqlWindowIdItem);
        }

        private void newPatientButtonClick(object sender, EventArgs e)
        {
            var registryForm = new RegistryForm(this.patientController);
            AddOwnedForm(registryForm);
            registryForm.Show();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            this.searchResultsDataGridView.Rows.Clear();
            var patient = new Patient(null, null);
            if (this.firstNameTextbox.Text.Equals(string.Empty) && this.lastNameTextBox.Text.Equals(string.Empty) &&
                this.birthDateChooser.Value.Equals(new DateTime(1753, 1, 1)))
            {
                MessageBox.Show("Must enter first name and last name or birthdate or both.");
                return;
            }

            if (!this.firstNameTextbox.Text.Equals(string.Empty))
            {
                patient.FirstName = this.firstNameTextbox.Text;
            }

            if (!this.lastNameTextBox.Text.Equals(string.Empty))
            {
                patient.LastName = this.lastNameTextBox.Text;
            }

            if (!this.birthDateChooser.Value.Equals(new DateTime(1753, 1, 1)))
            {
                patient.Birthdate = this.birthDateChooser.Value;
            }

            var patients = this.patientController.Search(patient);

            foreach (var patient1 in patients)
            {
                string[] row = {
                    patient1.PatientId.ToString(), patient1.FirstName, patient1.LastName,
                    patient1.Birthdate.ToShortDateString(), patient1.Gender.ToString(), patient1.Street,
                    patient1.City, patient1.State, patient1.Zip.ToString(), patient1.PhoneNumber.ToString()
                };
                this.searchResultsDataGridView.Rows.Add(row);
            }
        }

        public void SetUserOnMenuStrip(string userName)
        {
            this.userNameToolStripMenuItem.Text = "Welcome, " + userName;
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to log out?", "Confirm", MessageBoxButtons.YesNo) ==
                DialogResult.Yes)
            {
                Owner.Show();
                Hide();
            }
        }

        private void showConfirmationOnClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to log out?", "Confirm", MessageBoxButtons.YesNo) ==
                DialogResult.Yes)
            {
                Owner.Show();
                Hide();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void appointmentButton_Click(object sender, EventArgs e)
        {
            if (this.searchResultsDataGridView.SelectedRows.Count > 0)
            {
                var patientId = this.getSelectedPatientId();
                var appointmentForm = new AppointmentForm(patientId);
                AddOwnedForm(appointmentForm);
                appointmentForm.Show();
            }
        }

        private int getSelectedPatientId()
        {
            var row = this.searchResultsDataGridView.SelectedRows[0];
            return Convert.ToInt32(row.Cells["patientID"].Value);
        }

        private void routineCheckButton_Click(object sender, EventArgs e)
        {
            if (this.searchResultsDataGridView.SelectedRows.Count > 0)
            {
                var row = this.searchResultsDataGridView.SelectedRows[0];
                var patientId = Convert.ToInt32(row.Cells["patientID"].Value);
                var routineCheckForm = new RoutineCheckForm(patientId);
                AddOwnedForm(routineCheckForm);
                routineCheckForm.Show();
            }
        }

        private void rowSelected(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (e.StateChanged == DataGridViewElementStates.Selected)
            {
                this.appointmentButton.Enabled = true;
                this.visitInfoButton.Enabled = true;
                this.routineCheckButton.Enabled = true;
            }
            else
            {
                this.appointmentButton.Enabled = false;
                this.visitInfoButton.Enabled = false;
                this.routineCheckButton.Enabled = false;
            }
        }

        private void visitInfoButton_Click(object sender, EventArgs e)
        {
            if (this.searchResultsDataGridView.SelectedRows.Count > 0)
            {
                var patientId = this.getSelectedPatientId();
                var visitInfoForm = new VisitInfoForm(patientId);
                AddOwnedForm(visitInfoForm);
                visitInfoForm.Show();
            }
        }

        private void openSQLWindow(object sender, EventArgs e)
        {
            var sqlWindow = new AdminQueryForm();
            AddOwnedForm(sqlWindow);
            sqlWindow.Show();
        }

        #endregion
    }
}