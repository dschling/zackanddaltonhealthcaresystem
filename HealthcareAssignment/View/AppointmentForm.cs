﻿using System;
using System.Windows.Forms;
using HealthcareAssignment.Controller;
using HealthcareAssignment.Model;
using MySql.Data.MySqlClient;

namespace HealthcareAssignment.View
{
    public partial class AppointmentForm : Form
    {
        #region Data members

        private readonly AppointmentController appointmentController;
        private readonly int patient;
        private string errorMessage;

        #endregion

        #region Constructors

        public AppointmentForm(int patientId)
        {
            this.InitializeComponent();
            this.apoointmentTimePicker.Format = DateTimePickerFormat.Custom;
            this.apoointmentTimePicker.CustomFormat = "MM/dd/yyyy h:mm tt";
            this.apoointmentTimePicker.Value = DateTime.Now;
            this.appointmentController = new AppointmentController();
            this.populateDoctorsCombobox();
            this.patient = patientId;
        }

        #endregion

        #region Methods

        private void populateDoctorsCombobox()
        {
            foreach (var doctor in this.appointmentController.GetDoctorIDs())
            {
                this.doctorsComboBox.Items.Add(doctor);
            }
        }

        private void scheduleButton_Click(object sender, EventArgs e)
        {
            if (!this.containsValidInput())
            {
                MessageBox.Show(this.errorMessage);
            }
            else
            {
                try
                {
                    var apt = new Appointment();
                    var selectedDoc = (Doctor)this.doctorsComboBox.SelectedItem;
                    apt.DoctorID = selectedDoc.DoctorId;
                    apt.AppointmentDate = this.apoointmentTimePicker.Value;
                    apt.Reason = this.reasonTextBox.Text;
                    apt.PatientID = this.patient;
                    this.appointmentController.Add(apt);
                    MessageBox.Show("The appointment has successfully been scheduled");
                    this.resetToDefaultValues();
                }
                catch (MySqlException x)
                {

                    MessageBox.Show(x.Message);
                }
                
            }
        }

        private void resetToDefaultValues()
        {
            this.doctorsComboBox.ResetText();
            this.doctorsComboBox.SelectedIndex = -1;
            this.apoointmentTimePicker.Value = DateTime.Now;
            this.reasonTextBox.ResetText();
        }

        private bool containsValidInput()
        {
            this.errorMessage = "";
            var valid = true;
            if (this.doctorsComboBox.SelectedIndex < 0)
            {
                this.errorMessage += "*Must select a doctor" + Environment.NewLine;
                valid = false;
            }
            if (string.IsNullOrEmpty(this.reasonTextBox.Text))
            {
                this.errorMessage += "*Must enter a reason" + Environment.NewLine;
                valid = false;
            }
            return valid;
        }

        #endregion
    }
}